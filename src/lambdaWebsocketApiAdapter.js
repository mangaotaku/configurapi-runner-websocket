const Configurapi = require('configurapi');

const awsCallbackWaitsForEmptyEventLoop = 'aws-CallbackWaitsForEmptyEventLoop';


module.exports = {
    toResponse: function(response, context)
    {
        if(response.headers && awsCallbackWaitsForEmptyEventLoop in response.headers)
        {
            context.callbackWaitsForEmptyEventLoop = response.headers[awsCallbackWaitsForEmptyEventLoop];
        }

        let body = response.body;

        //Make JSON, if needed
        if(response.headers)
        {
            for(let headerKey of Object.keys(response.headers))
            {
                if(headerKey.toLowerCase() === 'content-type')
                {
                    let contentType = response.headers[headerKey];
                    if(contentType.includes('application/json'))
                    {
                        body = JSON.stringify(body);
                    }
                }
            }
        }

        return {
            statusCode: response.statusCode,
            headers: response.headers,
            body: body
        };
    },

    toRequest: function(awsRequest)
    {
        let request = new Configurapi.Request();

        request.method = (awsRequest.httpMethod || '').toLowerCase();
        awsRequest.body = JSON.parse(awsRequest.body);
        request.headers = {};

        for(let headerKey of Object.keys(awsRequest.headers || {}))
        {
            request.headers[headerKey.toLowerCase()] = awsRequest.headers[headerKey];
        }

        request.payload = awsRequest.body;
        request.query = {};
        for(let queryKey of Object.keys(awsRequest.multiValueQueryStringParameters || {}))
        {
            let value =  awsRequest.multiValueQueryStringParameters[queryKey].length === 1
                         ?
                         awsRequest.multiValueQueryStringParameters[queryKey][0]
                         :
                         awsRequest.multiValueQueryStringParameters[queryKey];

            request.query[queryKey.toLowerCase()] = value;
        }

        request.path = '';

        if(awsRequest.requestContext && awsRequest.requestContext.routeKey){
            if(awsRequest.requestContext.routeKey === '$connect'){
                request.path = '/v1/connect';
            }
            if(awsRequest.requestContext.routeKey === '$disconnect'){
                request.path = '/v1/disconnect';
            }
        }

        if(request.path === '' && awsRequest.body){
            request.path = request.body.action || '';
        }

        request.pathAndQuery = awsRequest.path || '';

        if(request.headers['content-type'] === 'application/json')
        {
            try
            {
                if(request.payload)
                {
                    request.payload = JSON.parse(request.payload);
                }
            }
            catch(err)
            {
                throw err;
            }
        }

        if(awsRequest.requestContext){
            let domain = awsRequest.requestContext.domain;
            let stage = awsRequest.requestContext.stage;
            let connectionId = awsRequest.requestContext.connectionId;
            request.params.connectionId = connectionId;
            let callbackUrl = `https://${domain}/${stage}/@connections/${connectionId}`;
            request.params.callback = callbackUrl;
        }

        return request;
    }
};
